AO X-ends for Misumi Leadscrew/Nut.  Work well with Mendelmax and Prism printers.

This is a modified version of the popular AO x-ends for use with Misumi leadscrew/flanged-nut combination.

There is one modification:
1) Leadscrew hole widened for MTSKR8 and bolt holes added.

Misumi Leadscrew: MTSKR8
Misumi Nut: MTSFR8

Assembly Instructions:
If you're not familiar with the x-axis of a Reprap, you can learn more at reprap.org/wiki/Mendel_X-axis.

Assembly is the same as AO Clamp X Ends (http://www.thingiverse.com/thing:15492) with these modifications:

a) Insert Misumi leadscrew through hole.  Then align the nut with the bolt holes closest to it.
b) Insert bolt with washer through top of X-end.
c) Use washer on nut on the bottom of leadscrew flange.
d) Tighten flange in X-end.
e) Finish assembly of X-end -- refer to http://www.thingiverse.com/thing:15492
f) Repeat for other X-end.

July 19, 2012:  Designed and tested footprint.  Revised bolt holes for alignment, and things look OK.  Printing for photos and assembly.