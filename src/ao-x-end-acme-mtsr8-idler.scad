use <x-clamp.scad>;
use <x-end-idler.scad>;
use <AO-bolted_pla_bushing.scad>;
include <nuts_and_bolts.scad>;

leadscrew_dia_misumi = 8;
leadscrew_nut_wrench_size_misumi = 15 + 1.15;		// plus some extra clearance
leadscrew_nut_height_misumi = 20;

//Added curves
module roundCorner(){
	translate([0,0,20]) difference(){
		translate([4,4,0]) cube([8,8,50], center=true);
		cylinder(r=8, h=50.1, center=true, $fn=20);
	}
}

//%translate([0.3,-2.5,-13.5]) xclamp();
//% translate([0,-6.5,-11.5]) rotate([0,0,180]) xendidler();
//#translate([0,-16,5]) bushing();
//#translate([0,-16,33.75]) bushing();

//X end block
difference(){
union(){
translate([0,-3,0]) cube([68.2,39.5,9], center=true);	//Main block
translate([0,13.5,0]) cylinder(r=19, h=9, center=true, $fn=20);
idlerWall();
}
	//Smooth rod grooves
	for (x= [24.945,-24.945]){
		translate([x,0,-4.495]) union() difference(){
			rotate([0,45,0]) translate([0,0,0]) cube([8.2,50,8.2], center=true);
			translate([0,0,-0.3]) difference(){
				translate([0,0,4.7]) rotate([0,45,0]) cube([2.05,51,2.05], center=true);
				translate([0,0,3.325]) rotate([90,0,0]) cylinder(r=2, h=53, $fn=20, center=true);
			}
		}
	}
	
	//clamp screw holes
	for (i = [ 	[16,9.5,0],
		[16,-16,0],
		[-16,9.5,0],
		[-16,-16,0]])
	{translate(i) cylinder(r=2, h=15, $fn=15, center=true);}
	for (i = [ 	[16,9.5,2.1],
		[16,-16,2.1],
		[-16,9.5,2.1],
		[-16,-16,2.1]])
	{translate(i) rotate([0,180,0]) boltHole(3, length=5);}

	//MTSNR8 leadscrew hole
	translate([0, 13.5, 0]) { 
		rotate([0, 0, 55]) { 
			cylinder(r=leadscrew_nut_wrench_size_misumi / 2, h=15, $fn=20, center=true);
			//MTSNR8 flange screw holes	
			for (i = [ 	[leadscrew_nut_wrench_size_misumi / 2 + 4, 0, 0],
				[-leadscrew_nut_wrench_size_misumi / 2 - 4, 0, 0]])
			{ translate(i) cylinder(r=2, h=15, $fn=15, center=true); }
		}
	}

	//Cut around nut
	//for (x=[27.84, -27.84]) translate([x,22.5,1.5]) cube([28,10,12.1], center=true);
	//translate([13.43,24.2,1.5]) rotate([0,0,-60]) cube([12,6,12.1], center=true);	
	//translate([-13.43,24.2,1.5]) rotate([0,0,60]) cube([12,6,12.1], center=true);

	//Smooth rod space
	translate([0,-20,0]) cube([15,8,10], center=true);
	translate([0,-16,0]) cylinder(r=7.5, h=10, center=true, $fn=25);

	//curves
		rotate([180,180,0]) union(){
		#translate([34.2-8,(49.5/2)-1.9-8,0]) roundCorner();
		#translate([-34.2+8,(49.5/2)-1.9-8,0]) rotate([0,0,90]) roundCorner();
		#translate([-34.2+8,-(49.5/2)+16-0.1,0]) rotate([0,0,-180]) roundCorner();
		#translate([34.2-8,-(49.5/2)+16-0.1,0]) rotate([0,0,-90]) roundCorner();
		}
		
		translate([7.5+4,-(39.5/2)+1,0]) difference(){
			translate([-2,-2,0]) cube([4,4,16.5], center=true);
			cylinder(r=4, h=16.8, center=true, $fn=20);
		}
		translate([-7.5-4,-(39.5/2)+1,0]) difference(){
			translate([2,-2,0]) cube([4,4,16.5], center=true);
			cylinder(r=4, h=16.8, center=true, $fn=20);
		}

}




//Bushing mount wall
difference(){
translate([0,-5.5,20+4.5]) cube([36,5.4,40.5], center=true);
for(i=[ 	[12,-6,39.25],
		[-12,-6,39.25],
		[12,-6,10.5],
		[-12,-6,10.5]
]){
translate(i) rotate([90,0,0]) cylinder(r=1.8, h=14, center=true, $fn=15);}
for(i=[ 	[12,-2.8,39.25],
		[-12,-2.8,39.25],
		[12,-2.8,10.5],
		[-12,-2.8,10.5]
]){
translate(i) rotate([90,0,0]) nutHole(3);}
}
// bushing mount triangle support
translate([0, -1.5, 0]) difference(){
translate([0, 2, 20 + 4]) cube([4,10,39], center=true);
translate([0, 11, 28]) rotate([14,0,0]) cube([4.1,20.1,50], center=true);
}


//Idler wall
module idlerWall(){
difference(){
	translate([34.1-(5.4/2),-3.0,(32.5/2)-1]) cube([5.4,39.5,39.5], center=true);
	translate([32,-3.1+4,4.5+16]) rotate([0,90,0]) cylinder(r=4.5, h=8, center=true, $fn=20);

//Fancy curves
	difference(){
		translate([34.1-(5.4/2)-3.1,-22.8,4.5+11.3]) cube([6.5,40.5,20]);
		translate([32,-3.1,4.5+11.3]) rotate([0,90,0]) cylinder(r=20, h=8, center=true, $fn=30);
	}
}
}
translate([25.5,-5.5,32.5]) cube([15,5.4,5], center=true);

