rodsize =8;
bushingOD = 16;
bushinglength = 11;
bushingthick = 2;
tablength = 10;
tabthick = 2.5;
holeR = 1.8;
holeNum = 2; // 1 or 2 holes

module bushing()
difference() {
linear_extrude(height = bushinglength, convexity = 5, center = false) difference() {
	union() {
		translate([0, bushingOD / 4, 0]) square([bushingOD, bushingOD / 2], center = true);
		circle(bushingOD / 2, center = true);
		translate([tablength/2 * (2-holeNum), bushingOD/2 - tabthick / 2, 0]) square([bushingOD + tablength * holeNum, tabthick], center = true);
	}
	rotate(45) square(rodsize, center = true);
	intersection() {
		for(a = [0:3]) rotate(45 + 90 * a) translate([(rodsize + bushingthick) / 2, (rodsize + bushingthick) / 2, 0]) square(bushingOD / 2, center = true);
		circle(bushingOD / 2 - bushingthick, center = true);
	}
	rotate(45 + 180) translate([(rodsize + bushingthick) / 2, (rodsize + bushingthick) / 2, 0]) square(bushingOD / 2, center = true);
}
//cube(size = [1,2,3], center = true);

translate([(bushingOD/2 + tablength/2) -1.7, bushingOD/2 - tabthick / 2, bushinglength / 2]) rotate([90,0,0]) cube(size = [6,3.5,6], center = true);

translate([(-bushingOD/2 - tablength/2) +1.7 , bushingOD/2 - tabthick / 2, bushinglength / 2]) rotate([90,0,0]) cube(size = [6,3.5,6], center = true);
}

count = 1;
for(i = [-count / 4 + 0.5: count / 4 - 0.5]) translate([i * (bushingOD + tablength * holeNum + 2), 0, 0]) for(j = [0, 1]) mirror([0, j, 0]) translate([0, -bushingOD / 2 - 3, 0]) bushing();