include <nuts_and_bolts.scad>;

leadscrew_dia_misumi = 8;
leadscrew_nut_wrench_size_misumi = 15 + 1.15;		// plus some extra clearance
leadscrew_nut_height_misumi = 20;

//Added curves
module roundCorner(){
	difference(){
		translate([4,4,0]) cube([8,8,16.5], center=true);
		cylinder(r=8, h=16.8, center=true, $fn=20);
	}
}

//X end block
rotate([180,0,0]) difference(){
union(){
translate([0,-3,1.5]) cube([68.2,39.5,12], center=true);	//Main block
translate([0,13.5,1.5]) cylinder(r=19, h=12, center=true, $fn=20);
	}
	//Smooth rod grooves
	for (x= [24.945,-24.945]){
		translate([x,0,-4.495]) union() difference(){
			rotate([0,45,0]) translate([0,0,0]) cube([8.2,50,8.2], center=true);
			translate([0,0,-0.3]) difference(){
				translate([0,0,4.7]) rotate([0,45,0]) cube([2.05,51,2.05], center=true);
				translate([0,0,3.325]) rotate([90,0,0]) cylinder(r=2, h=53, $fn=25, center=true);
			}
		}
	}

	//clamp screw holes
	for (i = [ 	[16,9.5,4.3],
		[16,-16,4.3],
		[-16,9.5,4.3],
		[-16,-16,4.3]])
	{translate(i) cylinder(r1=2, r2=3.4, h=2, $fn=25, center=true);}

	for (i = [ 	[16,9.5,3],
		[16,-16,3],
		[-16,9.5,3],
		[-16,-16,3]])
	{translate(i) cylinder(r=2, h=15, $fn=25, center=true);}
	
	//curves
	rotate([180,0,0]) union(){
	#translate([34.1-8,(49.5/2)-1.9-8,0]) roundCorner();
	#translate([-34.1+8-0.1,(49.5/2)-1.9-8,0]) rotate([0,0,90]) roundCorner();
	#translate([-34.1+8-0.1,-(49.5/2)+16-0.1,0]) rotate([0,0,-180]) roundCorner();
	#translate([34.1-8,-(49.5/2)+16-0.1,0]) rotate([0,0,-90]) roundCorner();
	}
	translate([7.5+4,-(39.5/2)+1,0]) difference(){
	translate([-2,-2,0]) cube([4,4,16.5], center=true);
	cylinder(r=4, h=16.8, center=true, $fn=20);
	}
	translate([-7.5-4,-(39.5/2)+1,0]) difference(){
	translate([2,-2,0]) cube([4,4,16.5], center=true);
	cylinder(r=4, h=16.8, center=true, $fn=20);
	}

	//M3 spaces
	for (i = [ 	[16,9.5,5.12],
		[16,-16,5.12],
		[-16,9.5,5.12],
		[-16,-16,5.12]])
	{translate(i) nutHole(3);}

	//MTSNR8 leadscrew hole
	translate([0, 13.5, 1.5]) { 
		rotate([0, 0, -55]) { 
			cylinder(r=leadscrew_nut_wrench_size_misumi / 2, h=15, $fn=20, center=true);
			//MTSNR8 flange screw holes	
			for (i = [ 	[leadscrew_nut_wrench_size_misumi / 2 + 4, 0, 0],
				[-leadscrew_nut_wrench_size_misumi / 2 - 4, 0, 0]])
			{ translate(i) cylinder(r=2, h=15, $fn=15, center=true); }
		}
	}

	//Smooth rod space
	translate([0,-20,1.5]) cube([15,8,12.1], center=true);
	translate([0,-16,1.5]) cylinder(r=7.5, h=12.1, center=true, $fn=30);

}

translate([0,0,2.1]) rotate([0,180,0]) boltHole(3, length=5);