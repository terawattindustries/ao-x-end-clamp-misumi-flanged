use <x-clamp.scad>;
use <x-end-motor.scad>;
use <AO-bolted_pla_bushing.scad>;
include <nuts_and_bolts.scad>;

leadscrew_dia_misumi = 8;
leadscrew_nut_wrench_size_misumi = 15 + 1.15;		// plus some extra clearance
leadscrew_nut_height_misumi = 20;

//%translate([0.3,-2.5,-13.5]) xclamp();
//%translate([0,-24.25,-11.5]) rotate([0,0,180]) xendmotor();
//#translate([0,-16,5]) bushing();
//#translate([0,-16,33.75]) bushing();

//Added curves
module roundCorner(){
	translate([0,0,20]) difference(){
		translate([4,4,0]) cube([8,8,50], center=true);
		cylinder(r=8, h=50.1, center=true, $fn=20);
	}
}

//X end block
difference(){
// round edge of leadscrew
union(){
translate([0,-3,0]) cube([68.2,39.5,9], center=true);	//Main block
translate([0,13.5,0]) cylinder(r=19, h=9, center=true, $fn=20);
}

	//Smooth rod grooves
	for (x= [24.945,-24.945]){
		translate([x,0,-4.495]) union() difference(){
			rotate([0,45,0]) translate([0,0,0]) cube([8.2,50,8.2], center=true);
			translate([0,0,-0.3]) difference(){
				translate([0,0,4.7]) rotate([0,45,0]) cube([2.05,51,2.05], center=true);
				translate([0,0,3.325]) rotate([90,0,0]) cylinder(r=2, h=53, $fn=20, center=true);
			}
		}
	}
	
	//clamp screw holes
	for (i = [ 	[16,8,0],
		[16,-16.5,0],
		[-16,8,0],
		[-16,-16.5,0]])
	{translate(i) cylinder(r=2, h=15, $fn=15, center=true);}
	for (i = [ 	[16,8,2.1],
		[16,-16.5,2.1],
		[-16,8,2.1],
		[-16,-16.5,2.1]])
	{translate(i) rotate([0,180,0]) boltHole(3, length=5);}

	//MTSNR8 leadscrew hole
	translate([0, 13.5, 0]) { 
		rotate([0, 0, 55]) { 
			cylinder(r=leadscrew_nut_wrench_size_misumi / 2, h=15, $fn=20, center=true);
			//MTSNR8 flange screw holes	
			for (i = [ 	[leadscrew_nut_wrench_size_misumi / 2 + 4, 0, 0],
				[-leadscrew_nut_wrench_size_misumi / 2 - 4, 0, 0]])
			{ translate(i) cylinder(r=2, h=15, $fn=15, center=true); }
		}
	}

	//Cut around nut
	//for (x=[22.84, -22.84]) translate([x,22.5,0]) cube([28,10,10], center=true);
	//translate([9.43,22.465,0]) rotate([0,0,-60]) cube([8,6,10], center=true);	
	//translate([-9.43,22.465,0]) rotate([0,0,60]) cube([8,6,10], center=true);

	//Smooth rod space
	translate([0,-20,0]) cube([15,8,10], center=true);
	translate([0,-16,0]) cylinder(r=7.5, h=10, center=true, $fn=25);

	//curves
		rotate([180,180,0]) union(){
		//#translate([34.2-8,(49.5/2)-1.9-8,0]) roundCorner();
		#translate([-34.2+8+0.01,(49.5/2)-1.9-8,0]) rotate([0,0,90]) roundCorner();
		#translate([-34.2+8+0.01,-(49.5/2)+16-0.1,0]) rotate([0,0,-180]) roundCorner();
		#translate([34.2-8,-(49.5/2)+16-0.1,0]) rotate([0,0,-90]) roundCorner();
		}
		
		translate([7.5+4,-(39.5/2)+1,0]) difference(){
			translate([-2,-2,0]) cube([4,4,16.5], center=true);
			cylinder(r=4, h=16.8, center=true, $fn=20);
		}
		translate([-7.5-4,-(39.5/2)+1,6.3]) difference(){
			translate([2,-2,0]) cube([4,4,16.5], center=true);
			cylinder(r=4, h=16.8, center=true, $fn=20);
		}

}


//Motor mount wall
difference(){
translate([-16.7,-20.25,-4.5]) bend();
	difference(){
	translate([-31.5,-53,25.2]) cube([6.5,38,35], center=true);
	translate([-31.5,-43.8,15]) cube([7,25,20], center=true);
	translate([-31.5,-37,22.5]) cube([7,10,10], center=true);
	translate([-31.5,-34.5,33.6]) rotate([0,90,0]) cylinder(r=8, h=7, center=true, $fn=25);
	translate([-31.5,-62.25,8]) rotate([0,90,0]) cylinder(r=8, h=7, center=true, $fn=25);
	}
translate([-6,-5,18]) cube([20,8,50], center=true);
translate([-25,0,0]) cube([12,45,9], center=true);

translate([-32,-48,20.5]) rotate([0,90,0]) union(){
	cylinder(r=11, h=8, $fn=25, center=true);

	rotate([0,0,45])
		for ( b  = [0 : 3])
		{
			rotate(b * 360 / 4, [0,0,1])
			translate([0,22,0])
				union(){
					for (y = [-2, 2])
					{
					translate([0,y,0])
					cylinder(r=1.75, h=8, $fn=10, center=true);
					}
				cube([3.5,4,8], center=true);
			}
		}
}

}
module bend(d1=10,d2=50,r=12,w=5.4,a=90,h=46)
{
  huge = r*w;
  difference()
  {
    cylinder(r=r+w, h=h);

    translate([0,0,-0.5]) 
    {
      cylinder(r=r, h=h+1);
      translate([0,r-huge,0]) cube([huge, huge, h+1]);
      rotate([0,0,-a]) translate([0,-r,0]) cube([huge, huge, h+1]);
    }
  }
  translate([0,r,0]) cube([d1, w, h]);
  rotate([0,0,-a]) translate([0,-(r+w),0]) cube([d2, w, h]);
}

difference(){
translate([-18.2,-46.25,-4.5+(2.5/2)]) cube([21.2,47.75,2.5], center=true);
translate([-7.8,-50,-4.5+(2.5/2)]) rotate([0,0,-22]) cube([20,60,3], center=true);
}



//Bushing mount wall
difference(){
translate([0,-5.5,20+4.5]) cube([36,5.4,40.5], center=true);
for(i=[ 	[12,-6,39.25],
		[-12,-6,39.25],
		[12,-6,10.5],
		[-12,-6,10.5]
]){
translate(i) rotate([90,0,0]) cylinder(r=1.8, h=14, center=true, $fn=15);}
for(i=[ 	[12,-2.8,39.25],
		[-12,-2.8,39.25],
		[12,-2.8,10.5],
		[-12,-2.8,10.5]
]){
translate(i) rotate([90,0,0]) nutHole(3);}
}
// bushing mount triangle support
translate([0, -1.5, 0]) difference(){
translate([0, 2, 20 + 4]) cube([4,10,39], center=true);
translate([0, 11, 28]) rotate([14,0,0]) cube([4.1,20.1,50], center=true);
}


//translate([-25,-50,1]) cube([20,20,5.65], center=true);

